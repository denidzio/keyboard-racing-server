import { IPlayer } from "./";

export default interface IRoom {
  name: string;
  players: IPlayer[];
  hasBeenStartedAt?: number;
  textId?: number;
  timerId?: NodeJS.Timeout;
}
