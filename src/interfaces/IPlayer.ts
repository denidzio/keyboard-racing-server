import { IUser } from "./";

export default interface IPlayer {
  user: IUser;
  isReady: boolean;
  inputtedText: string;
  progress: number;
  finishTime?: number;
}
