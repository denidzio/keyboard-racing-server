export default interface IUser {
  username: string;
  currentRoomName?: string;
}
