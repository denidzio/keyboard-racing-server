import { IUser } from "../interfaces";

let users: IUser[] = [];

export const getAllUsers = (): IUser[] => {
  return Object.assign([], users);
};

export const addUser = (user: IUser): IUser | null => {
  if (users.some((u) => u.username === user.username)) {
    return null;
  }

  users = [...users, user];
  return Object.assign({}, user);
};

export const getUser = (username: string): IUser | undefined => {
  const user = users.find((u) => u.username === username);

  if (!user) {
    return;
  }

  return Object.assign({}, user);
};

export const removeUser = (username: string): IUser | undefined => {
  const user = getUser(username);

  if (!user) {
    return;
  }

  users = users.filter((u) => u.username !== username);
  return Object.assign({}, user);
};

export const updateUser = (user: IUser): IUser | undefined => {
  if (!users.some((u) => u.username === user.username)) {
    return;
  }

  users = [...users.filter((u) => u.username !== user.username), user];
  return Object.assign({}, user);
};
