import { IRoom } from "../interfaces";

let rooms: IRoom[] = [];

export const getAllRooms = (): IRoom[] => {
  return Object.assign([], rooms);
};

export const addRoom = (room: IRoom): IRoom | null => {
  if (rooms.some((r) => r.name === room.name)) {
    return null;
  }

  rooms = [...rooms, room];
  return Object.assign({}, room);
};

export const getRoom = (name: string): IRoom | undefined => {
  const room = rooms.find((r) => r.name === name);

  if (!room) {
    return;
  }

  return Object.assign({}, room);
};

export const removeRoom = (name: string): IRoom | undefined => {
  const room = getRoom(name);

  if (!room) {
    return;
  }

  rooms = rooms.filter((r) => r.name !== name);
  return Object.assign({}, room);
};

export const updateRoom = (room: IRoom): IRoom | undefined => {
  if (!rooms.some((r) => r.name === room.name)) {
    return;
  }

  rooms = [...rooms.filter((r) => r.name !== room.name), room];
  return Object.assign({}, room);
};
