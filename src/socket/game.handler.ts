import { Socket } from "socket.io";
import { DefaultEventsMap } from "socket.io/dist/typed-events";

import { IGame, IRoom, IPlayer, IUser } from "../interfaces";
import { SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from "./config";
import { SECOND } from "../constants";
import { texts } from "../data";

import { getRoom, updateRoom } from "../db/room.db";

import {
  getUpdatedPlayersList,
  getPlayerByRoomAndUser,
} from "../helpers/player.helper";
import {
  getCleanRoom,
  getRoomWithUser,
  isRoomReady,
  mapRoomForClient,
} from "../helpers/room.helper";
import { getValidUser } from "../helpers/user.helper";
import { random } from "../helpers/random.helper";
import { didAllPlayersComplete, getGameResults } from "../helpers/game.helper";

export const gameOver = (
  io: {
    on?: (
      arg0: string,
      arg1: (
        socket: Socket<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
      ) => void
    ) => void;
    to?: any;
  },
  room: IRoom
) => {
  io.to(room.name).emit("GAME_IS_OVER", getGameResults(room.name));

  const updatedRoom = updateRoom(getCleanRoom(room));

  if (!updatedRoom) {
    return;
  }

  io.to(room.name).emit("UPDATE_ROOM", mapRoomForClient(updatedRoom));
};

export const startGame = (
  io: {
    on?: (
      arg0: string,
      arg1: (
        socket: Socket<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
      ) => void
    ) => void;
    to?: any;
  },
  room: IRoom
) => {
  const textId = random(0, texts.length - 1);

  room.hasBeenStartedAt = Date.now();
  room.textId = textId;

  const game: IGame = {
    textId,
    timerBeforeGame: SECONDS_TIMER_BEFORE_START_GAME,
    timerDuringGame: SECONDS_FOR_GAME,
  };

  io.to(room.name).emit("START_GAME", game);

  const timerId = setTimeout(() => {
    gameOver(io, room);
  }, (SECONDS_FOR_GAME + SECONDS_TIMER_BEFORE_START_GAME) * 1000);

  room.timerId = timerId;
};

export const togglePlayerState = (user: IUser) => {
  if (!user.currentRoomName) {
    return;
  }

  const room = getRoom(user.currentRoomName);

  if (!room || room.hasBeenStartedAt) {
    return;
  }

  const player = getPlayerByRoomAndUser(room, user);

  if (!player) {
    return;
  }

  const updatedPlayer: IPlayer = {
    ...player,
    isReady: !player.isReady,
  };

  const updatedRoom: IRoom = {
    ...room,
    players: getUpdatedPlayersList(room.players, updatedPlayer),
  };

  return updatedRoom;
};

export const handleTogglePlayerState = (
  socket: Socket<DefaultEventsMap, DefaultEventsMap>,
  io: {
    on?: (
      arg0: string,
      arg1: (
        socket: Socket<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
      ) => void
    ) => void;
    to?: any;
  }
) => {
  const user = getValidUser(socket.handshake.query.username);

  if (!user) {
    return;
  }

  const updatedRoom = togglePlayerState(user);

  if (!updatedRoom) {
    return;
  }

  io.to(updatedRoom.name).emit("UPDATE_ROOM", updatedRoom);

  if (isRoomReady(updatedRoom)) {
    startGame(io, updatedRoom);
  }

  updateRoom(updatedRoom);
};

export const handleInputChar = (
  socket: Socket<DefaultEventsMap, DefaultEventsMap>,
  io: {
    on?: (
      arg0: string,
      arg1: (
        socket: Socket<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
      ) => void
    ) => void;
    to?: any;
  },
  char: string
) => {
  const user = getValidUser(socket.handshake.query.username);

  if (!user) {
    return;
  }

  const room = getRoomWithUser(user);

  if (!room || room.textId === undefined || !room.hasBeenStartedAt) {
    return;
  }

  if (
    Date.now() - room.hasBeenStartedAt <
    SECONDS_TIMER_BEFORE_START_GAME * SECOND
  ) {
    return;
  }

  const player = getPlayerByRoomAndUser(room, user);

  if (!player) {
    return;
  }

  const text = texts[room.textId];

  if (char !== text[player.inputtedText.length]) {
    return;
  }

  player.inputtedText += char;
  player.progress = (100 * player.inputtedText.length) / text.length;

  if (player.progress === 100) {
    player.finishTime = Date.now();
  }

  room.players = getUpdatedPlayersList(room.players, player);

  io.to(room.name).emit("UPDATE_ROOM", mapRoomForClient(room));

  updateRoom(room);

  if (didAllPlayersComplete(room)) {
    gameOver(io, room);

    if (room.timerId) {
      clearTimeout(room.timerId);
    }
  }
};
