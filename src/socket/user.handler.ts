import { Socket } from "socket.io";
import { DefaultEventsMap } from "socket.io/dist/typed-events";

import { IUser } from "../interfaces";

import { addUser, removeUser } from "../db/user.db";
import { getAllRooms, getRoom, removeRoom, updateRoom } from "../db/room.db";

import { getValidUser, isUserUniq } from "../helpers/user.helper";
import { getAvailableRooms } from "../helpers/room.helper";
import { handleRoomExit } from "./room.handler";

export const handleUserConnect = (
  socket: Socket<DefaultEventsMap, DefaultEventsMap>
) => {
  const socketQuery = socket.handshake.query.username;

  if (!socketQuery) {
    return;
  }

  const newUser: IUser = { username: String(socketQuery) };

  if (!isUserUniq(newUser)) {
    socket.emit("USERNAME_IS_BUSY");
    socket.disconnect();
    return;
  }

  addUser(newUser);
  socket.emit("UPDATE_ROOMS", getAvailableRooms(getAllRooms()));
};

export const handleUserDisconnect = (
  socket: Socket,
  io: {
    on?: (
      arg0: string,
      arg1: (
        socket: Socket<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
      ) => void
    ) => void;
    to?: any;
  }
) => {
  const user = getValidUser(socket.handshake.query.username);

  if (!user) {
    return;
  }

  handleRoomExit(socket, io);
  removeUser(user.username);
};
