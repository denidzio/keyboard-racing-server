export const MAXIMUM_USERS_FOR_ONE_ROOM = 5;
export const SECONDS_TIMER_BEFORE_START_GAME = 10;
export const SECONDS_FOR_GAME = 60;

export const SERVER_CONFIG = {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
  },
};
