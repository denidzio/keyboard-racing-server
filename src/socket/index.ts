import { Socket } from "socket.io";
import { DefaultEventsMap } from "socket.io/dist/typed-events";

import { handleUserConnect, handleUserDisconnect } from "./user.handler";
import { handleTogglePlayerState, handleInputChar } from "./game.handler";
import {
  handleAddRoom,
  handleJoinToRoom,
  handleRoomExit,
} from "./room.handler";

export default (io: {
  on: (
    arg0: string,
    arg1: (
      socket: Socket<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
    ) => void
  ) => void;
}) => {
  io.on("connection", (socket: Socket) => {
    handleUserConnect(socket);

    socket.on("ADD_ROOM", (name) => handleAddRoom(socket, name));
    socket.on("JOIN_TO_ROOM", (name) => handleJoinToRoom(socket, io, name));
    socket.on("TOGGLE_PLAYER_STATE", () => handleTogglePlayerState(socket, io));
    socket.on("ROOM_EXIT", () => handleRoomExit(socket, io));
    socket.on("INPUT_CHAR", (char) => handleInputChar(socket, io, char));

    socket.on("disconnect", () => handleUserDisconnect(socket, io));
  });
};
