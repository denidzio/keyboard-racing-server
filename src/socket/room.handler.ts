import { Socket } from "socket.io";
import { DefaultEventsMap } from "socket.io/dist/typed-events";

import { IPlayer, IRoom, IUser } from "../interfaces";

import {
  addRoom,
  getAllRooms,
  getRoom,
  removeRoom,
  updateRoom,
} from "../db/room.db";
import { updateUser } from "../db/user.db";

import { getValidUser } from "../helpers/user.helper";
import {
  getAvailableRoom,
  getAvailableRooms,
  getRoomWithoutPlayer,
  isRoomReady,
  mapRoomForClient,
} from "../helpers/room.helper";
import { startGame } from "./game.handler";

export const handleAddRoom = (socket: Socket, name: string) => {
  if (!name) {
    return;
  }

  const user = getValidUser(socket.handshake.query.username);

  if (!user) {
    return;
  }

  const allRooms = getAllRooms();

  const updatedUser: IUser = {
    ...user,
    currentRoomName: name,
  };

  const newPlayer: IPlayer = {
    user: updatedUser,
    isReady: false,
    inputtedText: "",
    progress: 0,
  };

  const newRoom = addRoom({
    name,
    players: [newPlayer],
  });

  if (!newRoom) {
    socket.emit("ROOM_NAME_IS_BUSY");
    return;
  }

  socket.join(newRoom.name);
  socket.emit("JOIN_TO_ROOM", newRoom);
  socket.broadcast.emit("UPDATE_ROOMS", getAvailableRooms(allRooms));

  updateUser(updatedUser);
};

export const handleJoinToRoom = (
  socket: Socket,
  io: {
    on?: (
      arg0: string,
      arg1: (
        socket: Socket<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
      ) => void
    ) => void;
    to?: any;
  },
  name: string
) => {
  const user = getValidUser(socket.handshake.query.username);

  if (!user || (user && user.currentRoomName)) {
    return;
  }

  const room = getAvailableRoom(name);

  if (!room) {
    return;
  }

  const updatedUser: IUser = {
    ...user,
    currentRoomName: name,
  };

  const newPlayer: IPlayer = {
    user: updatedUser,
    isReady: false,
    inputtedText: "",
    progress: 0,
  };

  const updatedRoom = updateRoom({
    ...room,
    players: [...room.players, newPlayer],
  });

  if (!updatedRoom) {
    return;
  }

  socket.join(name);
  socket.emit("JOIN_TO_ROOM", updatedRoom);
  socket.broadcast.emit("UPDATE_ROOMS", getAvailableRooms(getAllRooms()));

  io.to(name).emit("UPDATE_ROOM", updatedRoom);

  updateUser({ ...user, currentRoomName: name });
};

export const handleRoomExit = (
  socket: Socket<DefaultEventsMap, DefaultEventsMap>,
  io: {
    on?: (
      arg0: string,
      arg1: (
        socket: Socket<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
      ) => void
    ) => void;
    to?: any;
  }
) => {
  const user = getValidUser(socket.handshake.query.username);

  if (!user || !user.currentRoomName) {
    return;
  }

  const room = getRoom(user.currentRoomName);

  if (!room) {
    return;
  }

  if (room.hasBeenStartedAt) {
    roomExitDuringGame(socket, room, user);
  } else {
    roomExitNotDuringGame(socket, io, room, user);
  }

  updateUser({ ...user, currentRoomName: undefined });

  socket.leave(room.name);
  socket.emit("UPDATE_ROOMS", getAvailableRooms(getAllRooms()));
  socket.broadcast.emit("UPDATE_ROOMS", getAvailableRooms(getAllRooms()));
};

export const roomExitDuringGame = (
  socket: Socket<DefaultEventsMap, DefaultEventsMap>,
  room: IRoom,
  user: IUser
) => {
  const cleanRoom = getRoomWithoutPlayer(room, user);

  if (cleanRoom.players.length === 0) {
    if (cleanRoom.timerId) {
      clearTimeout(cleanRoom.timerId);
    }

    removeRoom(cleanRoom.name);
    return;
  }

  socket.to(cleanRoom.name).emit("UPDATE_ROOM", mapRoomForClient(cleanRoom));
  updateRoom(cleanRoom);
};

export const roomExitNotDuringGame = (
  socket: Socket<DefaultEventsMap, DefaultEventsMap>,
  io: {
    on?: (
      arg0: string,
      arg1: (
        socket: Socket<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
      ) => void
    ) => void;
    to?: any;
  },
  room: IRoom,
  user: IUser
) => {
  const cleanRoom = getRoomWithoutPlayer(room, user);

  if (cleanRoom.players.length === 0) {
    removeRoom(cleanRoom.name);
    return;
  }

  socket.to(cleanRoom.name).emit("UPDATE_ROOM", mapRoomForClient(cleanRoom));

  if (isRoomReady(cleanRoom)) {
    startGame(io, cleanRoom);
  }

  updateRoom(cleanRoom);
};
