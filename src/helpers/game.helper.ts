import { getRoom } from "../db/room.db";
import { IRoom } from "../interfaces";

export const getGameResults = (roomName: string) => {
  const room = getRoom(roomName);

  if (!room) {
    return;
  }

  const players = room.players;

  return players.sort((a, b) => {
    if (!a.finishTime && !b.finishTime) {
      return b.progress - a.progress;
    }

    if (!a.finishTime && b.finishTime) {
      return 1;
    }

    if (a.finishTime && !b.finishTime) {
      return -1;
    }

    if (a.finishTime && b.finishTime) {
      return a.finishTime - b.finishTime;
    }

    return 0;
  });
};

export const didAllPlayersComplete = (room: IRoom) => {
  return room.players.every((p) => p.progress === 100);
};
