import { IRoom, IPlayer, IUser } from "../interfaces";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../socket/config";

import { getAllRooms, getRoom } from "../db/room.db";

export const getRoomWithUser = (user: IUser) => {
  return getAllRooms().find((room) =>
    room.players.find((p) => p.user.username === user.username)
  );
};

export const getRoomWithoutPlayer = (room: IRoom, user: IUser) => {
  return {
    ...room,
    players: room.players.filter((p) => p.user.username !== user.username),
  };
};

export const getAvailableRooms = (rooms: IRoom[]) => {
  return getAllRooms().filter(
    (room) =>
      !room.hasBeenStartedAt && room.players.length < MAXIMUM_USERS_FOR_ONE_ROOM
  );
};

export const isRoomReady = (room: IRoom) => {
  return room.players.every((p) => p.isReady === true);
};

export const getAvailableRoom = (name: string) => {
  const room = getRoom(name);

  if (!room || room.hasBeenStartedAt) {
    return;
  }

  return room;
};

export const mapRoomForClient = (room: IRoom) => {
  return { name: room.name, players: room.players };
};

export const getCleanRoom = (room: IRoom) => {
  const cleanPlayers: IPlayer[] = room.players.map((p: IPlayer) => ({
    user: p.user,
    isReady: false,
    inputtedText: "",
    progress: 0,
  }));

  const cleanRoom: IRoom = {
    name: room.name,
    players: cleanPlayers,
  };

  return cleanRoom;
};

// export const removePlayerFromRooms = (rooms: IRoom[], player: IPlayer) => {
//   const roomWithDesiredPlayer = findRoomWithPlayer(rooms, player);

//   if (!roomWithDesiredPlayer) {
//     return rooms;
//   }

//   roomWithDesiredPlayer.players = roomWithDesiredPlayer.players.filter(
//     (p) => p.username !== player.username
//   );
// };
