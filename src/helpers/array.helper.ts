export const checkUniqField = (arr: any[], field: string, value: any) => {
  return arr.find((item) => item[field] === value);
};
