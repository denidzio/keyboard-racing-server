import { IPlayer, IRoom, IUser } from "../interfaces";

export const getPlayerByRoomAndUser = (room: IRoom, user: IUser) => {
  return room.players.find((p) => p.user.username === user.username);
};

export const getUpdatedPlayersList = (players: IPlayer[], player: IPlayer) => {
  return [
    ...players.filter((p) => p.user.username !== player.user.username),
    player,
  ];
};
