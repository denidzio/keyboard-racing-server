import { IUser } from "../interfaces";

import { getAllUsers, getUser } from "../db/user.db";

export const getValidUser = (username: string | string[] | undefined) => {
  if (!username) {
    return;
  }

  return getUser(String(username));
};

export const isUserUniq = (user: IUser) => {
  return getAllUsers().every((u) => u.username !== user.username);
};
